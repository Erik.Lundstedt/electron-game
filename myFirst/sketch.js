var circle;  // Declare object

function setup() {
	createCanvas(765,530)
	// Create object
	circle = new ball();
}

function draw() {
	background(50, 89, 100);
	circle.move();
	circle.display();
}

// Jitter class
function ball() {
	this.x = random(width);
	this.y = random(height);
	this.diameter = random(20, 50);
	this.speed = 1;

	this.move = function()
	{
	this.x = mouseX;
	this.y = mouseY;
	};

	this.display = function() {
		ellipse(this.x, this.y, this.diameter, this.diameter);
	}
	function mousePressed() {
		circle.move();
		circle.display();
	}
};
